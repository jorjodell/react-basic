import React from 'react';
import ProductItem from './ProductItem';

function ProductList({ products, addToBasket }) {
  return (
    <div className="product-list">
      {products.map((item) => (
        <ProductItem
          key={item.id}
          product={item}
          onClick={addToBasket}
        />
      ))}
    </div>
  );
}

export default ProductList;