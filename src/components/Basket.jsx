import React from 'react';
import ProductItem from './ProductItem';

function Basket({ products, removeFromBasket, total }) {
  return (
    <section className="basket">
      <p>Количество товаров: {products.length}</p>
      <p>Общая сумма: {total}</p>
      {products.map((item) => (
        <ProductItem
          key={item.basketId}
          product={item}
          onClick={removeFromBasket}
        />
      ))}
    </section>
  );
}

export default Basket;