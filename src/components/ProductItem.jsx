import React from 'react';

function getParsedParams(parameters) {
  const paramsKeys = Object.keys(parameters);
  return paramsKeys.map((key) => {
    if(typeof parameters[key] === 'object') {
      return parameters[key].join(';');
    }
    return parameters[key];
  })
}

function ProductItem({ product, onClick }) {
  const { img, title, price, parameters } = product;
  return (
    <article className="product" onClick={() => onClick(product)}>
      <img src={img} className="product__img"/>
      <h1 className="product__title">{title}</h1>
      <p className="product__price">{price} сом</p>
      <ul>
        {getParsedParams(parameters).map((value, i) => (
          <li key={i}>{value}</li>
        ))}
      </ul>
    </article>
  );
}

export default ProductItem;