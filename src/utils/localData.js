export default [
  {
    id: 5673,
    title: 'Marvel Heroes',
    category: 'clothes',
    img: 'https://getthelabel.btxmedia.com/pws/client/images/catalogue/products/rfbts464nvyi/zoom/rfbts464nvyi_navy.jpg',
    price: 599,
    parameters: {
      type: 't-shirt',
      colors: ['red', 'blue', 'black'],
      sizes: ['S', 'M', 'L']
    }
  },
  {
    id: 2368,
    title: 'Iphone 11',
    category: 'phone',
    img: 'https://hotline.ua/img/tx/207/2070816675.jpg',
    price: 50000,
    parameters: {
      color: ['gold', 'grey'],
      memory: [64, 128, 256]
    }
  },
  {
    id: 3429,
    title: 'Star Wars',
    category: 'clothes',
    img: 'https://cdn.shopify.com/s/files/1/0034/8500/7936/products/DynamicImageHandler_d1286fd8-0329-498c-a2ee-94eede338876_2000x.png?v=1544223406',
    price: 1100,
    parameters: {
      type: 'hoodie',
      colors: ['white', 'black'],
      sizes: ['L', 'XL', 'XXL']
    }
  },
  {
    id: 1653,
    title: 'Идиот',
    category: 'book',
    img: 'https://cdn1.ozone.ru/s3/multimedia-7/c650/6008818891.jpg',
    price: 200,
    parameters: {
      author: 'Достоевский',
      pages: 467
    }
  },
  {
    id: 4002,
    title: 'Samsung S20',
    category: 'phone',
    img: 'https://andro-news.com/images/content/1702302.jpg',
    price: 80000,
    parameters: {
      color: ['blue', 'white'],
      memory: [64, 128]
    }
  },
  {
    id: 2920,
    title: '12 правил жизни',
    category: 'book',
    img: 'https://dimka.com/books/Jordan_Peterson-12_Rules_for_Life-ru-1.png',
    price: 620,
    parameters: {
      author: 'Джордан Питерсон',
      pages: 467
    }
  },

]