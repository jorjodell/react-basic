import React, { Component } from 'react';
import ProductList from './components/ProductList';
import Basket from './components/Basket';
import localData from './utils/localData';


class App extends Component {
  state = {
    products: localData,
    basket: [],
    basketTotal: 0,
  }

  addToBasket = (product) => {
    product.basketId = Date.now();
    this.setState({
      basket: [product, ...this.state.basket],
      basketTotal: this.state.basketTotal + product.price
    });
  }

  removeFromBasket = (product) => {
    const newBasket = this.state.basket.filter(
      (p) => p.basketId !== product.basketId
    );
    this.setState({
      basket: newBasket,
      basketTotal: this.state.basketTotal - product.price
    });
  }
  render() {
    return (
      <div className="App">
        <ProductList
          products={this.state.products}
          addToBasket={this.addToBasket}
        />
        <Basket
          products={this.state.basket}
          removeFromBasket={this.removeFromBasket}
          total={this.state.basketTotal}
        />
      </div>
    );
  }
}



export default App;